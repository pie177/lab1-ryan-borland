﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class FolderEditorScript : MonoBehaviour {

    //Makes a new item in the menu for the custom tool
    [MenuItem("Custom Tools/Folder Creator")]
    public static void CreateFolder()
    {
        //Tests if the function is running
        Debug.Log("Creating folders");

        //Creates all the folders needed
        AssetDatabase.CreateFolder("Assets", "Materials");
        AssetDatabase.CreateFolder("Assets", "Textures");
        AssetDatabase.CreateFolder("Assets", "Prefabs");
        AssetDatabase.CreateFolder("Assets", "Scripts");
        AssetDatabase.CreateFolder("Assets", "Scenes");
        AssetDatabase.CreateFolder("Assets", "Animations");
        AssetDatabase.CreateFolder("Assets/Animations", "AnimatonControllers");

        //Adds a text file in each folder describing the use of the folder
        System.IO.File.WriteAllText(Application.dataPath + "/Materials/folderStructure.txt", "This folder is for storing materials.");
        System.IO.File.WriteAllText(Application.dataPath + "/Textures/folderStructure.txt", "This folder is for storing textures.");
        System.IO.File.WriteAllText(Application.dataPath + "/Prefabs/folderStructure.txt", "This folder is for storing prefabs.");
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "This folder is for storing scripts.");
        System.IO.File.WriteAllText(Application.dataPath + "/Scenes/folderStructure.txt", "This folder is for storing scenes.");
        System.IO.File.WriteAllText(Application.dataPath + "/Animations/folderStructure.txt", "This folder is for storing raw animations.");
        System.IO.File.WriteAllText(Application.dataPath + "/Animations/AnimatonControllers/folderStructure.txt", "This folder is for storing animations.");

        //Refresh the project to apply all the changes
        AssetDatabase.Refresh();

    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
