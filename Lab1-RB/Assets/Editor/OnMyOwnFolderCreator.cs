﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class OnMyOwnFolderCreator : MonoBehaviour {

    // Creates a custom tool under the Custom Tools tab in the menu
    [MenuItem("Custom Tools/Full Folder Creator")]

    //The function that will run when the tool is used
    public static void CreateFolders()
    {
        // Creates the Dynamic Assets folder and all its sub-folders
        AssetDatabase.CreateFolder("Assets", "Dynamic Assets");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets", "Resources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animations");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animation Controllers");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Effects");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Models");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Enviroment");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Prefabs", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Sounds");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/SFX", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Textures");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Textures", "Common");

        // Creates asset folders
        AssetDatabase.CreateFolder("Assets", "Extensions");
        AssetDatabase.CreateFolder("Assets", "Gizmos");
        AssetDatabase.CreateFolder("Assets", "Plugins");
        AssetDatabase.CreateFolder("Assets", "Scripts");
        AssetDatabase.CreateFolder("Assets/Scripts", "Common");
        AssetDatabase.CreateFolder("Assets", "Shaders");

        // Creates the Static Assets folder and all its sub-folders
        AssetDatabase.CreateFolder("Assets", "Static Assets");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animations");
        AssetDatabase.CreateFolder("Assets/Static Assets/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animation Controllers");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Effects");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Models");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Enviroment");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Static Assets/Prefabs", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Scenes");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Sounds");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/SFX", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Textures");
        AssetDatabase.CreateFolder("Assets/Static Assets/Textures", "Common");

        // Creates a testing folder
        AssetDatabase.CreateFolder("Assets", "Testing");

        // Creates text files in the asset folders describing what they do
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets", "This folder are for assets that need to be in the game when running."); 
        System.IO.File.WriteAllText(Application.dataPath + "/Editor", "This is where editor scripts need to be.");
        System.IO.File.WriteAllText(Application.dataPath + "/Extensions", "This folder is for 3rd party assets//asset packages.");
        System.IO.File.WriteAllText(Application.dataPath + "/Gizmos", "This folder is for gizmo scripts.");
        System.IO.File.WriteAllText(Application.dataPath + "/Plugins", "This folder is for plugin scripts.");
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts", "This folder is for all the main scripts for the game.");
        System.IO.File.WriteAllText(Application.dataPath + "/Shaders", "This folder is for all the shader scripts");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets", "This folder is for static assets.");

        // Refreshes the system for the tool
        AssetDatabase.Refresh();
    }
}
